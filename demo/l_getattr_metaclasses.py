# In the face of ambiguity, refuse the temptation to guess.
# There should be one-- and preferably only one --obvious way to do it.
#
#                                               kek

class GetattrDemo:
    def __init__(self):
        self.answer = 42

    def __getattr__(self, item):
        return item


getattr_demo = GetattrDemo()
x = getattr_demo.answer
x = getattr_demo.cat
x = getattr_demo.dog8841


class GetattributeDemo:
    def __init__(self):
        self.answer = 42

    def __getattribute__(self, item):
        return item


getattribute_demo = GetattributeDemo()
x = getattribute_demo.answer
# same as above
x = GetattributeDemo.__getattribute__(getattribute_demo, "answer")
x = object.__getattribute__(getattribute_demo, "answer")


class Abomination:
    def __init__(self, **kwargs):
        self.proxies = kwargs

    def __getattr__(self, item):
        proxy, _, method = item.partition("_")
        try:
            return self.proxies[proxy].__getattribute__(method)
        except (KeyError, AttributeError):
            raise AttributeError(item)


abomination = Abomination(dict={}, list=[1, 2, 3], s="qwerty")
x = abomination.proxies
x = abomination.dict_update({"lol": 42})
x = abomination.proxies
x = abomination.dict_pop("lol")
x = abomination.proxies
x = abomination.list_pop()
x = abomination.proxies
x = abomination.s_split("er")


x = type(type) == type


class MetaDemo1:
    a = 42


class MetaDemo2:
    b = 43


def wrapped_repr(s):
    return repr(s)


class MoreStaticClass(MetaDemo1, MetaDemo2):
    c = 44
    d = wrapped_repr


more_static = MoreStaticClass()
x = more_static.a
x = more_static.b
x = more_static.c
x = more_static.d()


OnTheFlyClass = type("OnTheFlyClass", (MetaDemo1, MetaDemo2), {"c": 44, "d": wrapped_repr})

on_the_fly = OnTheFlyClass()
x = on_the_fly.a
x = on_the_fly.b
x = on_the_fly.c
x = on_the_fly.d()

class MyType(type):
    def __new__(mcs, name, bases, attrs):
        pass

# just google it if you are interested
