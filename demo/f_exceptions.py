
try:
    x = 10 / 0
except ZeroDivisionError as e:
    x = type(e)  # ZeroDivisionError
    x = x.__mro__  # Exceptions are objects
    x = e

try:
    raise x  # manually throw exception
except ZeroDivisionError:  # no temp assignment
    print("x was re-raised")

try:
    raise e
except (NameError, ZeroDivisionError) as e2:  # tuple of exception types
    # you could think of repr() as of extended str()
    print(repr(e2))  # which exception it catched?

try:
    try:
        1 / 0
    except ZeroDivisionError:
        pass
    print("Error was absorbed")
except ZeroDivisionError:
    print("Error was re-raised again")

try:
    try:
        1 / 0
    except ZeroDivisionError:
        raise  # throw an error being handled
    print("Error was absorbed")
except ZeroDivisionError:
    print("Error was re-raised again")

# multiple except. What would be printed?
try:
    raise ZeroDivisionError("Manually created exception")
except NameError:
    print("No x")
except (FileNotFoundError, ArithmeticError) as e:
    print("Arithmetic error or file not found (lol, what file?)")
    print(repr(e))
except ZeroDivisionError as e:
    print("Old ZeroDivisionError catched")
    print(repr(e))

x = issubclass(ZeroDivisionError, ArithmeticError)

try:
    raise ZeroDivisionError  # raise class instead of instance
except Exception as e:
    print(repr(e))

# Is Exception the very base class for all exceptions and errors?
try:
    raise 42
except Exception as e:
    print(repr(e))

try:
    raise BaseException
except Exception:
    print("Catched Exception")
except:  # same as except BaseException
    print("Catched BaseException")

x = issubclass(BaseException, Exception)
x = issubclass(Exception, BaseException)

# lets see why we need such a base
x = BaseException.__subclasses__()  # method not documented, do not use in production
# [<class 'Exception'>, - for normal exception
# <class 'GeneratorExit'>, - raises when generator makes .close(), you don't know what it s yet
# <class 'SystemExit'>, - raises when sys.exit() is called.
# <class 'KeyboardInterrupt'>] - when we stop application via console
# point of BaseException - to be the most exceptional exception and not being catch with `except Exception:`

# else - in case everything is fine
try:
    try:
        {}["some key"]
    except Exception as e:
        print(repr(e))
    else:
        print("Everything was find for the first time")
except Exception as e:
    print(repr(e))
else:
    print("Everything is fine")

# finally - will be executed no matter what
try:
    try:
        try:
            [][0]
        finally:
            print("First finally")  # exception pops up, but we do `finally` block
    except Exception as e:
        print(repr(e))  # exception handled and not popping up further
    finally:
        print("Second finally")  # but we still do `finally` block
finally:
    print("Third finally")  # no exception was raised or catched in corresponding `try` section,
                            # but we still do `finally`


class MyCuteException(Exception):
    pass


try:
    raise MyCuteException("It's so cute", 1, 2, 3, [], {}, False)
except MyCuteException as e:
    print(repr(e))


def handle_exceptions(f, *args, **kwargs):
    try:
        result = f(*args, **kwargs)
    except (KeyError, IndexError):
        print("Something wrong passed here -> [ ... ]")
    except Exception as e:
        print(repr(e))
    except ArithmeticError:
        print("This math of yours make me wanna die")
    else:
        print("Got result: {}".format(result))
        return result
    finally:
        print("Pretending cleaning up something")  # executes even when we return something inside try/except/else


def div(a, b):
    return a / b


# `or` returns first value not converted to False by bool()
x = handle_exceptions(div, 42, 2) or "Nothing"
x = handle_exceptions(div, 42, 0) or "Nothing"


def func1():
    return func2()


def func2():
    return func3()


def func3():
    return [][42]


x = handle_exceptions(func1) or "Nothing"


def func3():
    return "hey"


x = handle_exceptions(func1) or "Nothing"
