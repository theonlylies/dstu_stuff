import this


# want to do
def print_fib_less_than(max_fib):
    for n in fib(1000):
        if n >= max_fib:
            break
        print(n, end=" ")
    print()


def fib(count):
    if count < 1:
        return []
    if count == 1:
        return [1]
    res = [1, 1]
    for _ in range(count - 2):
        res.append(res[-1] + res[-2])
    return res


# problem: memory consumption to store all elements at once + calculation of elements > 1000, that we do not need
print_fib_less_than(5000)


# let`s try to fix problem above
class fib:
    def __init__(self, count):
        self.count = count
        self.last_pair = None, None

    def __iter__(self):
        return self

    def __next__(self):
        if self.count:
            self.count -= 1
            prev1, prev2 = self.last_pair
            current = 1 if prev1 is None or prev2 is None else prev1 + prev2
            self.last_pair = prev2, current
            return current
        raise StopIteration


# fine but code is a little sophisticated
print_fib_less_than(5000)


def fib(count):
    prev1, prev2 = 1, 1
    yield prev1
    yield prev2
    for _ in range(count - 2):
        prev1, prev2 = prev2, prev1 + prev2
        yield prev2


print_fib_less_than(5000)

x = type(fib)
# function should return one value
x = type(fib(123))

# here i ran out of a descent examples


def plus(a):
    print("Generator started")
    c = 0
    while True:
        try:
            c = a + (yield c)
        except Exception as e:
            print(repr(e))
            c = 0


plus_3 = plus(3)
x = plus_3.send(None)  # first always send None to start generator
x = plus_3.send(x)
x = plus_3.send(x)
x = plus_3.send(x / 6)
x = plus_3.throw(FileNotFoundError("Random exception"))


def only_take():
    print("Generator started")
    while True:
        print("Took value: {}".format((yield)))


ot = only_take()
print("Result value: {}".format(ot.send(None)))  # "Took value" not printed, because first None just started generator
                                                 # and frozen at yield expression
print("Result value: {}".format(ot.send(None)))
print("Result value: {}".format(ot.send(42)))


def freak(n1, delimeter, n2):
    yield from fib(n1)
    yield "|"
    yield from delimeter  # anything that support iteration protocol fits
    yield "|"
    yield from range(n2)


x = list(freak(5, "donkey!", 4))


# not so necessary to understand if you are not going to stuck in async netframework development

def dumb():
    print("Dumb started")
    yield 42
    yield "hey"
    return 8


def dumber():
    print("Dumber started")
    r = yield from dumb()
    yield from plus(r)

d = dumber()
print(d.send(None))
print(d.send(None))
# starts acting like plus()
print(d.send(None))  # giving last nothing to dumb() and take first value from plus()
print(d.send(1))
print(d.send(10))

pass
