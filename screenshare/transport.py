from socket import socket
from zlib import compress, decompress

def recvall(conn, length):
    buf = []
    received = 0
    while received < length:
        data = conn.recv(length - len(buf))
        if not data:
            return b""
        buf.append(data)
        received += len(data)
    return b"".join(buf)

def format_send(conn: socket, data: bytes):
    data = compress(data, 6)
    size = len(data).to_bytes(4, "big")
    conn.send(size)
    conn.sendall(data)

def format_recv(conn: socket) -> bytes:
    size = int.from_bytes(conn.recv(4), byteorder='big')
    data = recvall(conn, size)
    if not data:
        return b""
    return decompress(data)
