import pip
try:
    import requests
    import socks
except ImportError as e:
    print("installing missing modules")
    pip.main(["install", "requests", "pysocks"])

    import requests
    import socks


class TelegramBotApi:
    # https://core.telegram.org/bots/api
    BOT_URL_TEMPLATE = "https://api.telegram.org/bot{}/"

    def __init__(self, token, proxy=None):
        self.url = self.BOT_URL_TEMPLATE.format(token)
        self.proxies = {"https": proxy} if proxy else None

    def _request(self, http_method, telegram_method, **kwargs):
        response = requests.request(
            http_method,
            self.url + telegram_method,
            proxies=self.proxies,
            **kwargs
        )
        return response

    def _get(self, telegram_method, **method_params):
        return self._request("GET", telegram_method, params=method_params)

    def _post(self, telegram_method, **method_params):
        return self._request("POST", telegram_method, data=method_params)

    def get_updates(self, offset=0, timeout=0):
        return self._get("getUpdates", offset=offset, timeout=timeout).json()

    def send_message(self, chat_id, text):
        self._post("sendMessage", chat_id=chat_id, text=text)

